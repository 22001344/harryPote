//
//  MinhaCelula.swift
//  harryPote
//
//  Created by COTEMIG on 17/02/39 ERA1.
//

import UIKit

class MinhaCelula: UITableViewCell {

    @IBOutlet weak var personagem: UILabel!
    @IBOutlet weak var ator: UILabel!
    @IBOutlet weak var fotinha: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
