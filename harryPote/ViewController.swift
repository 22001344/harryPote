//
//  ViewController.swift
//  harryPote
//
//  Created by COTEMIG on 17/02/39 ERA1.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct Actor: Decodable{
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var actorList:[Actor]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actorList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MinhaCelula
        let actor = actorList![indexPath.row]
        
        cell.personagem.text = actor.name
        cell.ator.text = actor.actor
        cell.fotinha.kf.setImage(with: URL(string: actor.image))
        
        return cell
    }
    
    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        data()
        TableView.dataSource = self
    }
    
    private func data(){
        AF.request("https://hp-api.herokuapp.com/api/characters")
            .responseDecodable(of: [Actor].self){
            response in
                if let actor_list = response.value{
                    self.actorList = actor_list
            }
                self.TableView.reloadData()
        }
    }
}

